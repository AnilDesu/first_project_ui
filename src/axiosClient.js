import axios from "axios";
import cookie from "store";
import { useAuthStore } from "./stores/auth";
import { computed } from "vue";
const axiosClient = axios.create({
  baseURL: import.meta.env.VITE_API_BASE_URL,
  headers: {
    Authorization: `Bearer ${cookie.get("bearer")}`,
  },
});
export default axiosClient;
