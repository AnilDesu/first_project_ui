import cookie from "store";
import { defineStore } from "pinia";
import axiosClient from "../axiosClient";
import router from "../router/index.js";
import _ from "lodash";
import moment from "moment/moment";

export const useAnalyticsStore = defineStore("surveyAnalytics", {
  state: () => {
    return {
      surveyDetails: {
        pusblishedDate: null,
        title: null,
        views: 0,
        responsesCount: 0,
      },
      form_type: null,
      responsesList: [],
      questionsList: [],
    };
  },

  actions: {
    // get survey details
    async getSurveyDetails(surveyId) {
      const config = {
        headers: {
          Authorization: `Bearer ${cookie.get("bearer")}`,
          "Content-Type": "application/json",
        },
      };
      try {
        const response = await axiosClient.get(`/survey/${surveyId}`, config);
        this.surveyDetails.title = response.data.survey_title;
        this.surveyDetails.views = response.data.views;
        this.form_type = response.data.form_type;
      } catch (error) {
        console.log(error);
      }
    },

    async getPublishDate(surveyId) {
      const config = {
        headers: {
          Authorization: `Bearer ${cookie.get("bearer")}`,
          "Content-Type": "application/json",
        },
      };
      try {
        const response = await axiosClient.get(
          `/publish/publisher/${surveyId}`,
          config
        );
        if (response.status === 200) {
          this.surveyDetails.pusblishedDate = response.data.publish_date;
        }
      } catch (error) {
        console.log(error);
      }
    },

    // get responses
    async getResponsesBySurveyId(surveyId) {
      try {
        const response = await axiosClient.get(`user/survey/${surveyId}`, {
          headers: {
            Authorization: `Bearer ${cookie.get("bearer")}`,
            "Content-Type": "application/json",
          },
        });

        const transformedArray = _.sortBy(
          response.data.One_survey_users_data,
          (obj) => new Date(obj.timestamp)
        )
          .reverse()
          .map((obj) => {
            const transformedObj = _.omit(obj, "user_ans_list");
            const formattedTimestamp = moment(obj.timestamp).format(
              "DD-MMM-YYYY, HH:mm:ss"
            );
            return {
              ...transformedObj,
              timestamp: formattedTimestamp,
              ...obj.user_ans_list,
            };
          });
        this.responsesList = transformedArray;
        this.surveyDetails.responsesCount = transformedArray.length;
      } catch (error) {
        console.log(error);
      }
    },

    async getQuestionsList(surveyId) {
      this.currentView = "loading";
      const config = {
        headers: {
          Authorization: `Bearer ${cookie.get("bearer")}`,
          "Content-Type": "application/json",
        },
      };
      try {
        const response = await axiosClient.get(`/survey/${surveyId}`, config);
        // console.log(response.data.questions);
        if (response.status == 200) {
          this.questionsList = response.data.questions;
        }
      } catch (error) {
        console.log(error);
      }
    },
  },
});
