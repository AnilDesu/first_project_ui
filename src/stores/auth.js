import _ from "lodash";
import cookie from "store";
import { defineStore } from "pinia";
import axiosClient from "../axiosClient";
// import api from "@/plugins/api";
// import apiAuth from "@/plugins/api-auth";
import router from "../router/index.js";
export const useAuthStore = defineStore("auth", {
  state: () => {
    return {
      admin_id: cookie.get("adminId"),
      jwt_access_token: null,
      currentView: "success",
      signUpError: null,
      logInError: null,
      justSignedUp: false,
      passwordVerified: false,
      otpToken: null,
      forgotPasswordError: null,
      resetPasswordEmail: null,
      adminEmail: null,
      adminUsername: null,
    };
  },
  actions: {
    //signup methods
    async signup(user) {
      this.signUpError = null;
      this.adminEmail = user.admin_email;
      this.adminUsername = user.admin_username;
      try {
        const response = await axiosClient.post(
          "register",
          JSON.stringify(user),
          {
            headers: { "Content-Type": "application/json" },
          }
        );
        if (response.status == 201) {
          this.justSignedUp = true;
          router.push("/register/verify");
          // console.log(response);
        } else {
          this.signUpError =
            "Password does not meet policy requirements password must be include upper case, lower case, digit, and special characters";
          setTimeout(() => {
            this.signUpError = null;
          }, 4000);
        }
      } catch (error) {
        console.log(error);
        const message = _.get(error, "response.data.message");
        console.log(message);
        this.signUpError = message;

        setTimeout(() => {
          this.signUpError = null;
        }, 4000);
      }
    },

    // login  (password validation)
    async login(user) {
      this.currentStatus = "loading";
      this.logInError = null;
      try {
        const response = await axiosClient.post("login", JSON.stringify(user), {
          headers: { "Content-Type": "application/json" },
        });

        if (response.status == 200) {
          this.currentStatus = "success";
          this.otpToken = response.data.otp_token;
          this.passwordVerified = true;
          this.justSignedUp = false;
        }
        // console.log(response);
      } catch (error) {
        // const message = _.get(error, "response.data.message");
        console.log(error);
        if (error.response.status == 400) {
          this.logInError = error.response.data.message;
        } else {
          this.logInError = "invalid credentials";
        }

        setTimeout(() => {
          this.logInError = null;
        }, 3000);
      }
    },

    //login otp verification
    async verifyLogInOtp(otp) {
      try {
        const token = this.otpToken;
        const response = await axiosClient.post("login_with_otp", {
          token,
          otp,
        });
        this.admin_id = response.data.admin_id;
        this.jwt_access_token = response.data.jwt_access_token;
        this.passwordVerified = false;
        cookie.set("adminId", this.admin_id);
        cookie.set("bearer", this.jwt_access_token);
        router.push("/admin");
      } catch (error) {
        this.logInError = "Invalid Otp";
        setTimeout(() => {
          this.logInError = null;
        }, 3000);
      }
    },

    //logout
    async logout() {
      try {
        this.token = null;
        cookie.remove("bearer");
        cookie.remove("adminId");
        cookie.remove("surveyId");
        router.push("/");
      } catch (error) {
        console.log(error);
      }
    },

    //forgot password email verification
    async onVerifyEmail(data) {
      this.resetPasswordEmail = data.admin_email;
      try {
        const response = await axiosClient.post(
          "/admin/reset-password",
          JSON.stringify(data),
          {
            headers: { "Content-Type": "application/json" },
          }
        );
        console.log(response);

        if (response.status === 200) {
          router.push("/forgot/verify");
        }
      } catch (error) {
        console.log(error);
        if (error.response.status === 401) {
          this.forgotPasswordError = error.response.data.message;

          setTimeout(() => {
            this.forgotPasswordError = null;
          }, 5000);
        }
      }
    },

    //reset password
    async resetPassword(token, data) {
      try {
        const response = await axiosClient.post(
          `admin/reset-password/${token}`,
          data,
          {
            headers: { "Content-Type": "application/json" },
          }
        );
        // console.log(response);
        if (response.status === 200) {
          this.forgotPasswordError = response.data.message;
          setTimeout(() => {
            this.forgotPasswordError = null;
          }, 2000);

          router.push("/");
        }
      } catch (error) {
        // console.log(error.response.data.message);
        if (error.response.status === 400) {
          this.forgotPasswordError = error.response.data.message;
          // console.log(this.forgotPasswordError);
          setTimeout(() => {
            this.forgotPasswordError = null;
          }, 4000);
        }
      }
    },
  },
});
