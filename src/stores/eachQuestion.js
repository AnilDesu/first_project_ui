import cookie from "store";
import { defineStore } from "pinia";
import axiosClient from "../axiosClient";

export const useEachQuestion = defineStore("eachQuestion", {
  state: () => {
    return {};
  },
  actions: {
    //update category & update data based on Question id
    async updateQuestionCategory(id, data) {
      console.log(data, id);
      try {
        const response = await axiosClient.patch(
          `question/${id}`,
          JSON.stringify(data),
          {
            headers: {
              Authorization: `Bearer ${cookie.get("bearer")}`,
              "Content-Type": "application/json",
            },
          }
        );
        console.log(response);
      } catch (error) {
        console.log(error);
      }
    },
  },
});
