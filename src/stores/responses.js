import cookie from "store";
import { defineStore } from "pinia";
import axiosClient from "../axiosClient";
import router from "../router/index.js";
import _ from "lodash";
import moment from "moment/moment";
export const useResponsesStore = defineStore("responses", {
  state: () => {
    return {
      currentView: "loading",
      surveysData: {
        publishedList: [],
      },
      quizesData: {
        publishedList: [],
      },
      publishedSurveyIds: [],
      questionArray: [],
      noOfQuestions: null,
      formType: null,
      responsesList: [],
      headerList: [],
      commonHeaders: [
        {
          id: "email",
          question: "Email",
        },
        {
          id: "username",
          question: "Name",
        },
        {
          id: "timestamp",
          question: "Response time (24hrs)",
        },
      ],
    };
  },

  actions: {
    //get published surveys
    async getPublishedSurveys(admin_id) {
      try {
        const response = await axiosClient.get(`admin/publish/${admin_id}`, {
          headers: {
            Authorization: `Bearer ${cookie.get("bearer")}`,
            "Content-Type": "application/json",
          },
        });
        // console.log(response);
        if (response.status === 200 || response.data.length > 0) {
          this.publishedSurveyIds = _.map(response.data, "survey_id");
        }
      } catch (error) {
        console.log(error);
      }
    },
    // get responses
    async getResponsesBySurveyId(surveyId) {
      try {
        const response = await axiosClient.get(`user/survey/${surveyId}`, {
          headers: {
            Authorization: `Bearer ${cookie.get("bearer")}`,
            "Content-Type": "application/json",
          },
        });

        const transformedArray = _.sortBy(
          response.data.One_survey_users_data,
          (obj) => new Date(obj.timestamp)
        )
          .reverse()
          .map((obj) => {
            const transformedObj = _.omit(obj, "user_ans_list");
            const formattedTimestamp = moment(obj.timestamp).format(
              "DD-MMM-YYYY, HH:mm:ss"
            );
            return {
              ...transformedObj,
              timestamp: formattedTimestamp,
              ...obj.user_ans_list,
            };
          });
        this.responsesList = transformedArray;
      } catch (error) {
        console.log(error);
      }
    },
    async getQuestionsList(surveyId) {
      this.currentView = "loading";
      const config = {
        headers: {
          Authorization: `Bearer ${cookie.get("bearer")}`,
          "Content-Type": "application/json",
        },
      };
      try {
        const response = await axiosClient.get(`/survey/${surveyId}`, config);
        // console.log(response.data.questions);
        if (response.status == 200) {
          this.headerList = _.concat(
            this.commonHeaders,
            response.data.questions
          );
          this.formType = response.data.form_type;
        }
      } catch (error) {
        console.log(error);
        this.currentView = "failure";
      }
    },
    // get published surveys and quizes
    async getPublishedWorkSpaces(adminId, type) {
      const config = {
        headers: {
          Authorization: `Bearer ${cookie.get("bearer")}`,
          "Content-Type": "application/json",
        },
      };
      try {
        const response = await axiosClient.get(
          `survey/publish_data?admin_id=${adminId}&form_type=${type}`
        );
        console.log(response);
        if (response.status === 200) {
          if (type === "Quiz") {
            this.quizesData.publishedList = response.data.formTypeData;
          } else {
            this.surveysData.publishedList = response.data.formTypeData;
          }
        }
      } catch (error) {
        console.log(error);
      }
    },
  },
});
