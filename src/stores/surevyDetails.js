import cookie from "store";
import { defineStore } from "pinia";
import axiosClient from "../axiosClient";
import router from "../router/index.js";

export const useQuestionStore = defineStore("editQuestion", {
  state: () => {
    return {
      currentView: "loading",
      questionsList: [],
      form_type: null,
      surveyTitle: null,
      surveyDescription: null,
      surveyId: null,
      publishId: null,
      publishError: null,
    };
  },
  actions: {
    // get questions in survey and survey details
    async getQuestionsList(surveyId) {
      this.currentView = "loading";
      const config = {
        headers: {
          Authorization: `Bearer ${cookie.get("bearer")}`,
          "Content-Type": "application/json",
        },
      };
      try {
        // const response = await axiosClient.get(
        //   `/survey/${cookie.get("surveyId")}`,
        //   config
        // );
        const response = await axiosClient.get(`/survey/${surveyId}`, config);
        console.log(response);
        if (response.status === 200) {
          this.currentView = "success";
          this.questionsList = response.data.questions;
          this.form_type = response.data.form_type;
          this.surveyDescription = response.data.survey_description;
          this.surveyTitle = response.data.survey_title;
        }
      } catch (error) {
        console.log(error);
        this.currentView = "failure";
      }
    },

    //delete eachquestion
    async deleteQuestionById(id, surveyId) {
      try {
        const response = await axiosClient.delete(`/question/${id}`, {
          headers: {
            Authorization: `Bearer ${cookie.get("bearer")}`,
            "Content-Type": "application/json",
          },
        });
        if (response.status == 200) {
          this.getQuestionsList(surveyId);
        }
      } catch (error) {
        console.log(error);
      }
    },

    // publish survey by Id and get your unique id
    async publishSurveyById(data) {
      this.currentView = "loading";
      try {
        const response = await axiosClient.post("/publish", data, {
          headers: {
            Authorization: `Bearer ${cookie.get("bearer")}`,
            "Content-Type": "application/json",
          },
        });
        // console.log(response);
        if (response.status == 201) {
          this.currentView = "success";
          this.publishId = response.data.publish_id;
        }
      } catch (error) {
        console.log(error.response);

        this.currentView = "success";
        this.publishError = error.response.data.msg;
        if (
          error.response.status == 404 &&
          error.response.data.msg == "The survey id is already published"
        ) {
          // console.log(data.survey_id);
          this.publishIdOfSurveyId(data.survey_id);
        }
      }
    },

    //get publishId of surveyId
    async publishIdOfSurveyId(survey_id) {
      this.currentView = "loading";
      try {
        const response = await axiosClient.get(
          `/publish/publisher/${survey_id}`
        );
        if (response.status == 200) {
          this.currentView = "success";
          this.publishId = response.data.publish_id;
        }
      } catch (error) {
        console.log(error);
      }
    },
  },
});
