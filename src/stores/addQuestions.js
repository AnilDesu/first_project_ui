import cookie from "store";
import { defineStore } from "pinia";
import axiosClient from "../axiosClient";
import router from "../router/index.js";

export const useQuestionareStore = defineStore("addQuestions", {
  state: () => {
    return {
      currentView: "success", // possible views ....loading, success, failure, empty
      newQuestionsView: "empty", // possible views ....loading, success, failure, empty
      workSpace: {
        type: "Survey",
      },
      questionAction: null,
      questionFormVisible: false,
      questionsList: [],
      //add qn error
      addQnErr: false,
    };
  },
  actions: {
    // get questions in survey
    async getQuestionsList(questionareId) {
      try {
        const response = await axiosClient.get(`survey/${questionareId}`, {
          headers: {
            Authorization: `Bearer ${cookie.get("bearer")}`,
            "Content-Type": "application/json",
          },
        });
        if (response.status == 200) {
          this.questionsList = response.data.questions;
          if (response.data.questions.length == 0) {
            this.newQuestionsView = "empty";
          } else {
            this.newQuestionsView = "success";
          }
        }
      } catch (error) {
        console.log(error);
        this.newQuestionsView = "failure";
      }
    },

    // post new question
    async postQuestion(questionData, surveyId) {
      this.currentView = "loading";
      console.log(cookie.get("bearer"));
      try {
        const response = await axiosClient.post(
          "question",
          JSON.stringify(questionData),
          {
            headers: {
              Authorization: `Bearer ${cookie.get("bearer")}`,
              "Content-Type": "application/json",
            },
          }
        );
        if (response.status == 201) {
          this.currentView = "success";
          this.questionAction = null;
          this.questionFormVisible = false;
          this.getQuestionsList(surveyId);
        }
      } catch (error) {
        this.currentView = "failure";
        console.log(error);
      }
    },
  },
});
