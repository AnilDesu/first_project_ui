import cookie from "store";
import { defineStore } from "pinia";
import axiosClient from "../axiosClient";
import router from "../router/index.js";

export const useHomeStore = defineStore("adminHome", {
  state: () => {
    return {
      currentView: "loading", // possible views are 'loading', 'success', 'failure', 'empty'
      surveys: [],
      adminName: "",
      surveysData: {
        survyesList: [],
      },
      quizsData: {
        quizsList: [],
      },
      newWorkSpace: {
        action: null,
        type: null,
      },
      // new questionare Id
      newQuestionareId: null,
      newSurveyTitle: null,
      //questionares errors
      isQuestionresError: false,
      questionareError: null,
      //new questionare
      isNewQuestionareError: false,
      newQuestionareError: null,
      surveyEditAction: null,
      formType: null,
      // logo
      logoDetails: {
        logoAction: null,
        showlogo: true,
      },
    };
  },
  actions: {
    // get questionares list by admin
    async getQuestionares(adminId) {
      this.isQuestionresError = false;
      this.currentView = "loading";
      const config = {
        headers: {
          Authorization: `Bearer ${cookie.get("bearer")}`,
          "Content-Type": "application/json",
        },
      };
      try {
        const response = await axiosClient.get(`admin/${adminId}`, config);
        if (response.status == 200) {
          this.adminName = response.data.admin_username;
          this.surveys = response.data.surveys;
          this.currentView =
            response.data.surveys.length === 0 ? "empty" : "success";
        }
      } catch (error) {
        console.log(error);
        this.currentView = "failure";
      }
    },

    // get surveys or quizs by params
    async getRelatedForms(adminId, formType) {
      const config = {
        headers: {
          Authorization: `Bearer ${cookie.get("bearer")}`,
          "Content-Type": "application/json",
        },
      };
      try {
        const response = await axiosClient.get(
          `survey/form_type?admin_id=${adminId}&form_type=${formType}`
        );
        if (response.status == 200) {
          if (formType === "Survey") {
            this.surveysData.survyesList = response.data.formTypeData;
          } else {
            this.quizsData.quizsList = response.data.formTypeData;
          }
        }
      } catch (error) {
        console.log(error);
      }
    },
    // add new questionare
    async addNewSurvey(data) {
      this.newQuestionareError = false;
      try {
        const response = await axiosClient.post("survey", data, {
          headers: {
            Authorization: `Bearer ${cookie.get("bearer")}`,
            "Content-Type": "multipart/form-data",
          },
        });

        if (response.status == 201) {
          this.newSurveyTitle = response.data.survey_title;
          this.newQuestionareId = response.data.id;
          this.formType = response.data.form_type;
          cookie.set("surveyId", this.newQuestionareId);
          // router.push(`admin/surveys/${response.data.id}`);
          router.push("/admin/surveyoptions");
        }
        // console.log(response);
      } catch (error) {
        this.newQuestionareError = true;
        this.questionareError = error.response.data;

        setTimeout(() => {
          this.newQuestionareError = false;
          this.questionareError = null;
        }, 5000);
      }
    },
    // update survey details by id
    async updateSurveyDetails(data, surveyId, adminId) {
      try {
        const response = await axiosClient.patch(`/survey/${surveyId}`, data, {
          headers: {
            Authorization: `Bearer ${cookie.get("bearer")}`,
            "Content-Type": "multipart/form-data",
          },
        });
        if (response.status == 200) {
          this.surveyEditAction = false;
          this.logoDetails.showlogo = true;
          this.getQuestionares(cookie.get("adminId"));
        }
      } catch (error) {
        console.log(error);
        // failure status -404
      }
    },
    // delete survey by id
    async deleteSurveyById(surveyId, type) {
      // this.currentView = "loading";
      try {
        const response = await axiosClient.delete(`survey/delete/${surveyId}`, {
          headers: { Authorization: `Bearer ${cookie.get("bearer")}` },
        });
        if (response.status == 200) {
          this.getRelatedForms(cookie.get("adminId"), type);
          // this.getQuestionares(cookie.get("adminId"));
        }
      } catch (error) {
        console.log(error);
      }
    },

    // delete survey logo
    async deleteLogoSurveyById(surveyId) {
      // this.currentView = "loading";
      try {
        const response = await axiosClient.delete(
          `delete_upload_image/${surveyId}`,
          {
            headers: { Authorization: `Bearer ${cookie.get("bearer")}` },
          }
        );
        // console.log(response);
        if (response.status === 200) {
          this.logoDetails.showlogo = false;
        }
      } catch (error) {
        console.log(error);
      }
    },
  },
});
