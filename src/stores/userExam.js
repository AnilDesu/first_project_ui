import cookie from "store";
import { defineStore } from "pinia";
import axiosClient from "../axiosClient";

export const userStore = defineStore("userExam", {
  state: () => {
    return {
      currentView: "loading",
      questionsList: [],
      form_type: "Survey",
      title: null,
      description: null,
      logo: null,
    };
  },
  actions: {
    //get questions
    async getQuestionsList(surveyId, uuid) {
      this.currentView = "loading";
      try {
        const response = await axiosClient.get(
          `questions/${surveyId}/${uuid} `,
          {
            headers: {
              // Authorization: `Bearer ${cookie.get("bearer")}`,
              "Content-Type": "application/json",
            },
          }
        );
        if (response.status == 200) {
          this.currentView = "success";
          this.logo = response.data.logo_url;
          this.title = response.data.survey_title;
          this.description = response.data.survey_description;
          this.questionsList = response.data.questions;
          this.form_type = response.data.form_type;
          this.updateViews(surveyId);
        }
      } catch (error) {
        console.log(error);
        this.currentView = "failure";
      }
    },

    // update views
    async updateViews(surveyId) {
      try {
        const response = await axiosClient.post(`survey/${surveyId}`, {
          headers: {
            "Content-Type": "application/json",
          },
        });
      } catch (error) {
        console.log(error);
      }
    },

    //post answers
    async postUserData(data) {
      console.log(data);
      this.currentView = "loading";
      try {
        const response = await axiosClient.post(`/user`, data);
        // console.log(response);
        if (response.status == 201) {
          this.currentView = "success";
        }
      } catch (error) {
        console.log(error);
      }
    },
  },
});
