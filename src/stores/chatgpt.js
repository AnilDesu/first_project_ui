import cookie from "store";
import { defineStore } from "pinia";
import axiosClient from "../axiosClient";
import router from "../router/index.js";
// import api from "@/plugins/api";
// import apiAuth from "@/plugins/api-auth";

export const useChatgpt = defineStore("chatgpt", {
  state: () => {
    return {
      currentView: "initial", // possible views are 'loading', 'success', 'failure', 'empty'
      questionsList: [],
      error: null,
      showError: null,
      fetchingStatus: false,
    };
  },
  actions: {
    // get survey by openai
    async getQuestionsByChatgpt(data, surveyId) {
      this.currentView = "loading";
      this.fetchingStatus = true;
      this.error = null;
      this.showError = false;
      const config = {
        headers: {
          Authorization: `Bearer ${cookie.get("bearer")}`,
          "Content-Type": "application/json",
        },
      };
      try {
        const response = await axiosClient.post(
          "chat",
          JSON.stringify(data),
          config
        );
        if (response.status === 200) {
          this.questionsList = response.data;
          this.currentView = "success";

          if (this.questionsList.length > 0) {
            router.push(`surveys/${surveyId}`);
          }
        }
        console.log(response);
      } catch (error) {
        console.log(error);
        this.currentView = "failure";
        this.fetchingStatus = false;
        this.error = "Something went wrong";
        this.showError = true;
        setTimeout(() => {
          this.error = null;
          this.showError = false;
        }, 8000);
      }
    },

    async getQuestionsForQuizByChatgpt(data, quizId) {
      this.currentView = "loading";
      this.fetchingStatus = true;
      this.error = null;
      this.showError = false;
      const config = {
        headers: {
          Authorization: `Bearer ${cookie.get("bearer")}`,
          "Content-Type": "application/json",
        },
      };
      try {
        const response = await axiosClient.post(
          "chat_gpt_quiz",
          JSON.stringify(data),
          config
        );
        console.log(response);
        if (response.status === 200) {
          this.questionsList = response.data;
          this.currentView = "success";

          if (this.questionsList.length > 0) {
            router.push(`surveys/${quizId}`);
          }
        }
        console.log(response);
      } catch (error) {
        console.log(error);
        this.currentView = "failure";
        this.fetchingStatus = false;
        this.error = "Something went wrong";
        this.showError = true;
        setTimeout(() => {
          this.error = null;
          this.showError = false;
        }, 8000);
      }
    },
  },
});
