import { defineStore } from "pinia";

export const useAppStore = defineStore("app", {
  state: () => {
    return {
      menubarOpen: false,
    };
  },
  actions: {
    async toggleMenubar() {
      this.menubarOpen = !this.menubarOpen;
    },
    async closeMenubar() {
      this.menubarOpen = false;
    },
  },
});
