import axios from "axios";

const apiAuth = axios.create({
  baseURL: "https://turiyaform-api-test.onrender.com/",
});

export default apiAuth;
