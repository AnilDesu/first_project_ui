export default [
  "General Knowledge",
  "Language and Literature",
  "Science and Technology",
  "Arts and Culture",
  "Sports",
  "Entertainment",
  "Business and Economics",
  "Health & Wellbeing",
];
