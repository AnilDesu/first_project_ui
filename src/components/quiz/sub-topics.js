export const subTopics = (mainTopic) => {
  if (mainTopic === "General Knowledge") {
    return [
      {
        topic: "World Geography",
        prompt:
          "Questions about countries, capitals, landmarks, continents, and geographical features.",
      },
      {
        topic: "History",
        prompt:
          "Quizzes on historical events, famous personalities, ancient civilizations, and significant milestones.",
      },
      {
        topic: "Science and Technology",
        prompt:
          "Questions about basic scientific principles, inventions, discoveries, space exploration, and technological advancements.",
      },
      {
        topic: "Literature and Authors",
        prompt:
          "Quizzes on famous books, authors, literary genres, and literary quotes.",
      },
      {
        topic: "Art and Culture",
        prompt:
          "Questions about famous paintings, artists, art movements, music genres, and cultural traditions.",
      },
      {
        topic: "Sports",
        prompt:
          "Quizzes on various sports, sports personalities, major tournaments, and Olympic history.",
      },
      {
        topic: "Politics and Current Affairs",
        prompt:
          "Questions about political systems, world leaders, international organizations, and recent news events.",
      },
      {
        topic: "Mythology and Folklore",
        prompt:
          "Quizzes on Greek mythology, Norse mythology, folklore from different cultures, and mythical creatures.",
      },
      {
        topic: "Famous Personalities",
        prompt:
          "Questions about influential figures in various fields such as science, politics, arts, and entertainment.",
      },
      {
        topic: "General Science",
        prompt:
          "Quizzes covering topics like biology, chemistry, physics, astronomy, and scientific principles.",
      },
    ];
  } else if (mainTopic === "Language and Literature") {
    return [
      {
        topic: "Grammar and Vocabulary",
        prompt:
          "Quizzes on grammar rules, parts of speech, sentence structure, word meanings, synonyms, and antonyms.",
      },
      {
        topic: "Literary Genres",
        prompt:
          "Questions about different genres such as fiction, non-fiction, poetry, drama, mystery, science fiction, and romance.",
      },
      {
        topic: "Famous Authors and Works",
        prompt:
          "Quizzes on renowned authors, their notable literary works, and their contributions to literature.",
      },
      {
        topic: "Literary Devices",
        prompt:
          "Questions about literary devices like metaphor, simile, personification, alliteration, irony, and symbolism.",
      },
      {
        topic: "Quotes and Quotations",
        prompt:
          "Quizzes that test knowledge of famous quotes from literature and their respective authors.",
      },
      {
        topic: "Poetry Analysis",
        prompt:
          "Questions on understanding and interpreting poetic elements such as rhyme, meter, imagery, and themes in poems.",
      },
      {
        topic: "Classic Literature",
        prompt:
          "Quizzes on well-known classic literary works, including novels, plays, and epic poems from different time periods.",
      },
      {
        topic: "Literary History",
        prompt:
          "Questions about important literary movements, periods, and significant literary figures throughout history.",
      },
      {
        topic: "Literary Characters",
        prompt:
          "Quizzes on memorable characters from literature, their traits, and their roles within their respective stories.",
      },
      {
        topic: "Language and Cultural References",
        prompt:
          "Language and Cultural References: Questions about idioms, proverbs, cultural references, and linguistic expressions from different languages.",
      },
    ];
  } else if (mainTopic === "Science and Technology") {
    return [
      {
        topic: "General Science Knowledge",
        prompt:
          "Quizzes covering fundamental concepts in physics, chemistry, biology, and earth sciences.",
      },
      {
        topic: "Space and Astronomy",
        prompt:
          "Questions about planets, moons, stars, galaxies, space exploration missions, and astronomical phenomena.",
      },
      {
        topic: "Technology and Gadgets",
        prompt:
          "Quizzes on modern technologies, electronic devices, computer hardware, software, and advancements in the tech industry.",
      },
      {
        topic: "Human Anatomy and Physiology",
        prompt:
          "Questions about the human body systems, organs, functions, and common health-related topics.",
      },
      {
        topic: "Computer Science and Programming",
        prompt:
          "Quizzes on programming languages, algorithms, data structures, software development concepts, and computer hardware.",
      },
      {
        topic: "Environmental Science",
        prompt:
          "Questions about ecosystems, environmental issues, climate change, renewable energy sources, and conservation practices.",
      },
      {
        topic: "Genetics and Biotechnology",
        prompt:
          "Quizzes on genetic inheritance, DNA structure, biotechnological applications, genetic engineering, and cloning.",
      },
      {
        topic: "Medical Science",
        prompt:
          "Questions about diseases, medical terminology, medical procedures, healthcare advancements, and medical history.",
      },
      {
        topic: "Robotics and Artificial Intelligence",
        prompt:
          "Quizzes on robotics, AI technologies, machine learning, neural networks, and autonomous systems.",
      },
      {
        topic: "Innovations and Scientific Discoveries",
        prompt:
          "Questions about groundbreaking inventions, scientific breakthroughs, notable scientists, and their contributions.",
      },
    ];
  } else if (mainTopic === "Arts and Culture") {
    return [
      {
        topic: "Visual Arts",
        prompt:
          "Quizzes on famous paintings, artists, art movements, art history, and art techniques.",
      },
      {
        topic: "Music",
        prompt:
          "Questions about music genres, famous composers, notable musicians, musical instruments, and music theory.",
      },
      {
        topic: "Film and Cinema",
        prompt:
          "Quizzes on famous films, directors, actors, film genres, memorable movie quotes, and film industry trivia.",
      },
      {
        topic: "Theater and Drama",
        prompt:
          "Questions about famous playwrights, theater history, well-known plays, notable actors, and theatrical techniques.",
      },
      {
        topic: "Literature",
        prompt:
          "Quizzes on famous books, authors, literary genres, literary devices, and literary movements.",
      },
      {
        topic: "Architecture",
        prompt:
          "Questions about iconic buildings, architectural styles, famous architects, and architectural history.",
      },
      {
        topic: "Dance",
        prompt:
          "Quizzes on various dance forms, famous dancers, choreographers, dance styles, and dance terminology.",
      },
      {
        topic: "Cultural Traditions",
        prompt:
          "Questions about customs, rituals, festivals, traditional clothing, and cultural practices from different regions.",
      },
      {
        topic: "Art Movements",
        prompt:
          "Quizzes on artistic movements such as Renaissance, Impressionism, Surrealism, Cubism, and Pop Art.",
      },
      {
        topic: "Cultural Icons",
        prompt:
          "Questions about influential figures in arts and culture, including musicians, artists, actors, writers, and cultural leaders.",
      },
    ];
  } else if (mainTopic === "Sports") {
    return [
      {
        topic: "Football (Soccer)",
        prompt:
          "Quizzes on football history, famous players, international tournaments, club competitions, and football trivia.",
      },
      {
        topic: "Basketball",
        prompt:
          "Questions about basketball rules, NBA history, iconic players, teams, championships, and basketball trivia.",
      },
      {
        topic: "Tennis",
        prompt:
          "Quizzes on tennis rules, Grand Slam tournaments, famous tennis players, tennis history, and tennis trivia.",
      },
      {
        topic: "Cricket",
        prompt:
          "Questions about cricket rules, international teams, famous cricketers, cricket World Cups, and cricket history.",
      },
      {
        topic: "Olympic Games",
        prompt:
          "Quizzes on the history of the Olympics, host cities, notable moments, Olympic sports, and record-breaking athletes.",
      },
      {
        topic: "Baseball",
        prompt:
          "Questions about baseball rules, Major League Baseball (MLB) teams, baseball history, famous players, and baseball trivia.",
      },
      {
        topic: "Athletics (Track and Field)",
        prompt:
          "Quizzes on various athletic events, world records, Olympic champions, and notable athletes in track and field.",
      },
      {
        topic: "Motorsports",
        prompt:
          "Questions about Formula 1, NASCAR, MotoGP, famous drivers, racing circuits, and motorsports trivia.",
      },
      {
        topic: "Winter Sports",
        prompt:
          "Quizzes on winter sports like skiing, snowboarding, ice hockey, figure skating, Winter Olympics, and winter sports trivia.",
      },
      {
        topic: "Sports Trivia",
        prompt:
          "Questions covering a wide range of sports, including general sports knowledge, famous moments, records, and sports history.",
      },
    ];
  } else if (mainTopic === "Entertainment") {
    return [
      {
        topic: "Movies",
        prompt:
          "Movies: Quizzes on film trivia, famous actors and actresses, movie quotes, film genres, and award-winning films.",
      },
      {
        topic: "Television Shows",
        prompt:
          "Television Shows: Questions about popular TV series, characters, plotlines, TV genres, and memorable TV moments.",
      },
      {
        topic: "Music",
        prompt:
          "Music: Quizzes on music genres, famous musicians and bands, song lyrics, album covers, and music history.",
      },
      {
        topic: "Celebrities",
        prompt:
          "Celebrities: Questions about famous personalities from the entertainment industry, their careers, achievements, and personal lives.",
      },
      {
        topic: "Books and Authors",
        prompt:
          "Books and Authors: Quizzes on popular books, renowned authors, literary awards, bestsellers, and book-to-film adaptations.",
      },
      {
        topic: "Gaming",
        prompt:
          "Gaming: Questions about video games, gaming consoles, gaming history, popular game franchises, and gaming trivia.",
      },
      {
        topic: "Comics and Superheroes",
        prompt:
          "Comics and Superheroes: Quizzes on comic book characters, superhero movies, comic book history, and comic book trivia.",
      },
      {
        topic: "Reality TV",
        prompt:
          "Reality TV: Questions about popular reality TV shows, contestants, hosts, reality show formats, and reality TV moments.",
      },
      {
        topic: "Awards and Events",
        prompt:
          "Awards and Events: Questions about prestigious awards like the Oscars, Emmys, Grammys, and notable entertainment events throughout history.",
      },
    ];
  } else if (mainTopic === "Business and Economics") {
    return [
      {
        topic: "Business Concepts",
        prompt:
          "Business Concepts: Quizzes on fundamental business terms, principles, and concepts like supply and demand, marketing, branding, and entrepreneurship.",
      },
      {
        topic: "Economics",
        prompt:
          "Economics: Questions about macroeconomics, microeconomics, economic theories, market structures, fiscal and monetary policies, and economic indicators.",
      },
      {
        topic: "Financial Management",
        prompt:
          "Financial Management: Quizzes on financial statements, budgeting, financial ratios, investment strategies, risk management, and financial markets.",
      },
      {
        topic: "International Business",
        prompt:
          "International Business: Questions about global trade, multinational corporations, international finance, foreign exchange, and global economic trends.",
      },
      {
        topic: "Business Ethics",
        prompt:
          "Business Ethics: Quizzes on ethical decision-making, corporate social responsibility, sustainability practices, and business ethics frameworks.",
      },
      {
        topic: "Management and Leadership",
        prompt:
          "Management and Leadership: Questions about management principles, leadership styles, organizational behavior, team dynamics, and strategic management.",
      },
      {
        topic: "Entrepreneurship",
        prompt:
          "Entrepreneurship: Quizzes on starting and managing a business, business planning, market analysis, innovation, and entrepreneurial mindset.",
      },
      {
        topic: "Marketing and Advertising",
        prompt:
          "Marketing and Advertising: Questions about marketing strategies, consumer behavior, branding, advertising techniques, and digital marketing.",
      },
      {
        topic: "Corporate Finance",
        prompt:
          "Corporate Finance: Quizzes on financial planning, capital budgeting, valuation, mergers and acquisitions, and corporate governance.",
      },
      {
        topic: "Economic History",
        prompt:
          "Economic History: Questions about economic systems, historical economic events, economic revolutions, and influential economists throughout history.",
      },
    ];
  } else if (mainTopic === "Health & Wellbeing") {
    return [
      {
        topic: "Nutrition",
        prompt:
          "Nutrition: Quizzes on various aspects of nutrition, such as macronutrients, micronutrients, vitamins, and minerals.",
      },
      {
        topic: "Exercise and Fitness",
        prompt:
          "Exercise and Fitness: Quizzes on various types of exercise, such as cardio, strength training, and yoga. It can also generate quizzes on fitness topics such as muscle groups, workout routines, and more.",
      },
      {
        topic: "Mental Health",
        prompt:
          "Mental Health: Quizzes on mental health topics, such as anxiety, depression, stress, and more.",
      },
      {
        topic: "Health Conditions",
        prompt:
          "Health Conditions: Quizzes on various health conditions such as diabetes, heart disease, cancer, and more.",
      },
      {
        topic: "Millets",
        prompt:
          "Millets: Quizzes on the benefits of millets, the different types of millets available, how to cook with millets, and more.",
      },
      {
        topic: "Organic Food",
        prompt:
          "Organic Food: Quizzes on the benefits of organic food, how to identify organic food, and more.",
      },
      {
        topic: "Cold Press Oil",
        prompt:
          "Cold Press Oil: Quizzes on the benefits of cold press oil, the different types of cold press oils available, how to use them in cooking, and more.",
      },
      {
        topic: "Wellness Practices",
        prompt:
          "Wellness Practices: Questions about mindfulness, meditation, relaxation techniques, sleep hygiene, time management, and stress reduction.",
      },
      {
        topic: "Alternative Therapies",
        prompt:
          "Alternative Therapies: Quizzes on complementary and alternative medicine practices, natural remedies, holistic approaches to health, and traditional healing methods.",
      },
      {
        topic: "Human Anatomy",
        prompt:
          "Human Anatomy: Questions about the structure and function of the human body systems, organs, and major physiological processes.",
      },
    ];
  } else {
    return [];
  }
};
