export default [
  {
    name: "Customer Satisfaction",
    code: "Customer satisfaction survey in JSON format: A survey that aims to measure how satisfied customers are with a product or service",
  },
  {
    name: "Employee engagement survey",
    code: "Employee engagement survey in JSON format: A survey that aims to measure how engaged employees are in their work",
  },
  {
    name: "Market research survey",
    code: "Market research survey in JSON format: A survey that aims to collect data about a particular market or industry",
  },
  {
    name: "Event feedback survey",
    code: "Event feedback survey in JSON format: A survey that aims to collect feedback from attendees after an event",
  },
  {
    name: "Website Feedback Survey",
    code: "Website feedback survey in JSON format: A survey that aims to collect feedback from website visitors about the website's design, usability, and content",
  },
  {
    name: "Product feedback survey",
    code: "Product feedback survey in JSON format: A survey that aims to collect feedback from users about a specific product",
  },
  {
    name: "Net Promoter Score (NPS) survey",
    code: "Net Promoter Score (NPS) survey in JSON format: A survey that aims to measure customer loyalty by asking customers how likely they are to recommend a product or service to others",
  },
];
