import cookie from "store";
import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/user/:id/:uuid",
    name: "user",
    // component: () => import("../views/UserExam.vue"),
    component: () => import("../pages/enduser/quiz/chatbot.vue"),
  },
  {
    path: "/signup",
    name: "signup",
    component: () => import("../views/SignUp.vue"),
  },
  {
    path: "/register/verify",
    name: "register",
    component: () => import("../views/VerifyEmailPage.vue"),
  },
  {
    path: "/",
    name: "login",
    component: () => import("../views/LoginPage.vue"),
    meta: {
      requiresGuest: true,
    },
  },
  {
    path: "/forgot",
    name: "forgot",
    component: () => import("../views/ForgotPassword.vue"),
  },
  {
    path: "/reset_password/:token",
    name: "reset",
    component: () => import("../views/ResetPasswordPage.vue"),
  },
  {
    path: "/forgot/verify",
    name: "verify",
    component: () => import("../views/VerifyResetEmailPage.vue"),
  },

  {
    path: "/admin",
    name: "adminPage",

    children: [
      {
        path: "/admin",
        name: "adminPage",
        component: () => import("../pages/admin/Home.vue"),
      },
      {
        path: "/admin/surveyoptions",
        name: "surveySelection",
        component: () => import("../views/Chatgpt/SurveySelection.vue"),
      },
      {
        path: "/admin/surveys/:id",
        name: "surveyDetails",
        component: () => import("../views/Questions.vue"),
      },
      {
        path: "/admin/responses",
        name: "surveyResponse",
        component: () => import("../pages/admin/Responses.vue"),
      },
      {
        path: "/admin/:id/analytics",
        name: "surveyAnalytics",
        component: () => import("../views/SurveyAnalytics.vue"),
      },
    ],

    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/thankspage",
    name: "thnakspage",
    component: () => import("../components/SutableJobs.vue"),
  },
];

// check the authentication by cookie
const isAuthenticated = () => {
  const jwtToken = cookie.get("bearer");
  return Boolean(jwtToken);
};

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  if (
    to.matched.some((record) => record.meta.requiresAuth) &&
    !isAuthenticated()
  ) {
    next({ path: "/", query: { redirect: to.fullPath } });
  } else if (
    to.matched.some((record) => record.meta.requiresGuest) &&
    isAuthenticated()
  ) {
    next("/admin");
  } else {
    next();
  }
});

export default router;
