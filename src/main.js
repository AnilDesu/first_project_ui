import { createApp } from "vue";
import router from "./router/index.js";
import { createPinia } from "pinia";
import "./style.css";
import App from "./App.vue";
import "@jsonforms/vue-vanilla/vanilla.css";
/* import the fontawesome core */
import { library } from "@fortawesome/fontawesome-svg-core";
/* import font awesome icon component */
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
/* import specific icons */
import {
  faUserSecret,
  faArrowRight,
  faArrowRightLong,
  faPrint,
  faXmark,
  faAnglesRight,
  faRightFromBracket,
  faHome,
  faEllipsisVertical,
  faArrowUpRightFromSquare,
  faKeyboard,
  faCircleHalfStroke,
  faRocket,
  faCopy,
  faPlus,
  faChevronCircleLeft,
  faTrashCan,
  faAngleLeft,
  faDownload,
  faChartSimple,
} from "@fortawesome/free-solid-svg-icons";
import PrimeVue from "primevue/config";

// add css
import "primevue/resources/themes/lara-light-blue/theme.css";
import "primevue/resources/primevue.min.css";
import "primeicons/primeicons.css";

//prime css
import "primevue/resources/themes/lara-light-blue/theme.css";

import {
  faEye,
  faUser,
  faCircleCheck,
  faPaperPlane,
  faEnvelope,
  faStar,
} from "@fortawesome/free-regular-svg-icons";

/* add icons to the library */
library.add(
  faUserSecret,
  faArrowRight,
  faArrowRightLong,
  faPrint,
  faEye,
  faXmark,
  faPlus,
  faUser,
  faRocket,
  faAnglesRight,
  faRightFromBracket,
  faHome,
  faPaperPlane,
  faCopy,
  faTrashCan,
  faChevronCircleLeft,
  faAngleLeft,
  faDownload,
  faChartSimple,
  faEllipsisVertical,
  faArrowUpRightFromSquare,
  faKeyboard,
  faCircleHalfStroke,
  faCircleCheck,
  faKeyboard,
  faCircleHalfStroke,
  faEnvelope,
  faStar
);

// importing components
import Navbar from "./components/Navbar.vue";
import Sidebar from "./components/Sidebar.vue";
import CreateQuestion from "./components/CreateQuestion.vue";

//ui components
import Hamburger from "./components/ui/Hamburger.vue";

/* add prime vue components*/
import Image from "primevue/image";
import ProgressBar from "primevue/progressbar";
import InputText from "primevue/inputtext";
import InputNumber from "primevue/inputnumber";
import Checkbox from "primevue/checkbox";
import FileUpload from "primevue/fileupload";
import Paginator from "primevue/paginator";
import Password from "primevue/password";
import RadioButton from "primevue/radiobutton";
import Dropdown from "primevue/dropdown";
import Button from "primevue/button";
import Message from "primevue/message";
import ProgressSpinner from "primevue/progressspinner";
import Textarea from "primevue/textarea";
import Rating from "primevue/rating";
import Tooltip from "primevue/tooltip";
import TabMenu from "primevue/tabmenu";
import TabView from "primevue/tabview";
import TabPanel from "primevue/tabpanel";
import ConfirmPopup from "primevue/confirmpopup";
import Dialog from "primevue/button";
import DynamicDialog from "primevue/dynamicdialog";
import DialogService from "primevue/dialogservice";

import Chart from "primevue/chart";

import ConfirmDialog from "primevue/confirmdialog";
import Toast from "primevue/toast";
import ToastService from "primevue/toastservice";
import ConfirmationService from "primevue/confirmationservice";

const pinia = createPinia();

const app = createApp(App);

app.use(ToastService);
app.use(ConfirmationService);
app.use(DialogService);

// directive
app.directive("tooltip", Tooltip);

// componets
app.component("Paginator", Paginator);
app.component("font-awesome-icon", FontAwesomeIcon);
app.component("ProgressBar", ProgressBar);
app.component("InputText", InputText);
app.component("InputNumber", InputNumber);
app.component("Image", Image);
app.component("Password", Password);
app.component("FileUpload", FileUpload);
app.component("Checkbox", Checkbox);
app.component("Dropdown", Dropdown);
app.component("Button", Button);
app.component("UiButton", Button);
app.component("Message", Message);
app.component("ProgressSpinner", ProgressSpinner);
app.component("Textarea", Textarea);
app.component("TabMenu", TabMenu);
app.component("TabPanel", TabPanel);
app.component("TabView", TabView);
app.component("ConfirmDialog", ConfirmDialog);
app.component("Dialog", Dialog);
app.component("Toast", Toast);
app.component("Chart", Chart);
app.component("Rating", Rating);
//admin components
app.component("Navbar", Navbar);
app.component("CreateQuestion", CreateQuestion);
app.component("Sidebar", Sidebar);

// ui components
app.component("Hamburger", Hamburger);

// packages and render
app.use(pinia);
app.use(router);
app.use(PrimeVue, { ripple: true });

app.mount("#app");
